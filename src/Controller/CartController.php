<?php

namespace App\Controller;

use DateTime;
use App\Entity\Panier;
use App\Entity\Produit;
use App\Form\PanierType;
use App\Service\Cart\CartService;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CartController extends AbstractController
{
    /**
     * @Route("/panier", name="cart_index")
     * 
     */
    public function index(CartService $cartService,
    Request $request, ObjectManager $manager)
    { 

      $panier = new Panier();

      $form = $this->createForm(PanierType::class, $panier);
      $form->handleRequest($request);

      if($form->isSubmitted() && $form->isValid()){
        $panier->setCreatedAt(new DateTime());
       // $panier->setTotal($this->getTotal());
        $panier->setUser($this->getUser());
        $manager->persist($panier);
        $manager->flush();

        $this->addFlash(
          'success',
          'Votre commande a bien été enregistreé'
        );
   
        // on le passe a twig pour l'afficher 
        return $this->redirectToRoute("paiement_index");
        
      }
        return $this->render('cart/cart.html.twig', [
        
            'items' => $cartService->getFullCart(),
            'total' => $cartService->getTotal(),
            'formpanier' => $form->createView(),
      
        ]);
      
     }
    

    /**
     * Permet de ajuter un produit aux panier 
     * 
     * @Route("/panier/add/{id}", name="cart_add")
     * @return Response
     */

    public function add($id,CartService $cartService, ObjectManager $manager, 
                Request $request) {
        //j'ai utilisé les injections indipendances pour demander directement à Symfony de 
        //me rendre le produit 
        //ensuite je récupére la methode add() qui se trouve dans la cartService
           $cartService->add($id);
         // on fait redirige ver la liste 
           return $this->redirectToRoute("cart_index");
       }
 
      
      /**
       * Permet de supprimer un produit
       * @Route("/panier/remove/{id}", name="cart_remove")
       */
      public function remove($id, CartService $cartService) {
          
          // remove permet de supprimer le produit de mon panier
          $cartService->remove($id);
          // redirection vers la liste 
          return $this->redirectToRoute("cart_index");
        }
        
        /**
         * @Route("/paiement", name="paiement_index")
         */
        
        public function paiement(CartService $cartService){

          $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        
          return $this->render('paiement/paiement.html.twig', [
            'total' => $cartService->getTotal()
        ]);
        }
  
    }
        
  
        
   
