<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="page_accueil")
     */
      public function accueil()
    {
        return $this->render('home/accueil.html.twig');
    }

     /**
     * @Route("/mentionlegal", name="mentionlegal_index")
     */
       public function montionLegal()
    {
        return $this->render('mentionlegal/index.html.twig', [
            'controller_name' => 'MentionlegalController',
        ]);
    }

    /**
     * @Route("/apropo/de/nous", name="apropos_deNous")
     */
    public function ApropoDeNous(){
        return $this->render('home/apropodenous.html.twig');

    }

   

   
}
